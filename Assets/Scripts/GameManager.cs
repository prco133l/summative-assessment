using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject ballPrefab;
    public GameObject playerPrefab;
    public UnityEngine.UI.Text scoreText;
    public UnityEngine.UI.Text ballsText;
    public UnityEngine.UI.Text levelText;
    public UnityEngine.UI.Text highscoreText;

    public GameObject panelMenu;
    public GameObject panelPlay;
    public GameObject panelLevelComplete;
    public GameObject panelGameOver;

    public GameObject[] levels;

    public static GameManager Instance { get; private set;}

    public enum State {MENU, INIT, PLAY, LEVELCOMPLETED, LOADLEVEL, GAMEOVER}
    State _state;
    GameObject _currentBall;
    GameObject _currentlevel;
    bool _isSwitchingState;
    GameObject _Ball;

    public AudioSource Complete;
    public AudioSource GameOver;


    private int _score;
    public int Score
    {
        get { return _score; }
        set { _score = value; 
            scoreText.text = "SCORE: " + _score;
        }
    }
    
    private int _level;
    public int Level
    {
        get { return _level; }
        set { _level = value; 
            levelText.text = "LEVEL: " + (1 + _level);
        }
    }

    private int _balls;
    public int Balls
    {
        get { return _balls; }
        set { _balls = value; 
            ballsText.text = "MUSHROOMS: " + _balls;
        }
    }
    
    
    public void PlayClicked()
    {
        SwitchState(State.INIT);
    }
    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
        SwitchState(State.MENU);
    }

    public void SwitchState(State newState, float delay = 0)
    {
        StartCoroutine(SwitchDelay(newState, delay));
    }

    IEnumerator SwitchDelay(State newState, float delay = 0)
    {
        _isSwitchingState =  true;
        yield return new WaitForSeconds(delay);
        EndState();
        _state = newState;
        BeginState(newState);
        _isSwitchingState = false;
    }

    void BeginState(State newState)
    {
        switch (newState)
        {
            case State.MENU:
                Cursor.visible = true;
                highscoreText.text = "HIGHSCORE: " + PlayerPrefs.GetInt("highscore");
                panelMenu.SetActive(true);
                break;
            case State.INIT:
                Cursor.visible = false;
                panelPlay.SetActive(true);
                Score = 0;
                Level = 0;
                Balls = 3;
                if(_currentlevel != null)
                {
                    Destroy(_currentlevel);
                }
                _Ball = Instantiate(playerPrefab);
                SwitchState(State.LOADLEVEL);
                break;
            case State.PLAY:
                break;
            case State.LEVELCOMPLETED:
                AudioSource.PlayClipAtPoint(Complete.clip, Camera.main.transform.position, 0.08f);
                Destroy(_currentBall);
                Destroy(_currentlevel);
                Level++;
                panelLevelComplete.SetActive(true);
                SwitchState(State.LOADLEVEL, 2f);
                break;
            case State.LOADLEVEL:
                if( Level >= levels.Length)
                {
                    SwitchState(State.GAMEOVER);
                }
                else
                {
                    _currentlevel = Instantiate(levels[Level]);
                    SwitchState(State.PLAY);
                }
                break;
            case State.GAMEOVER:
            AudioSource.PlayClipAtPoint(GameOver.clip, Camera.main.transform.position, 0.08f);
            if( Score > PlayerPrefs.GetInt("highscore"))
            {
                PlayerPrefs.SetInt("highscore", Score);
            }
                panelGameOver.SetActive(true);
                break;
        }
    }
    // Update is called once per frame
    void Update()
    {
        switch (_state)
        {
            case State.MENU:
                break;
            case State.INIT:
                break;
            case State.PLAY:
                if(_currentBall == null)
                {
                    if(Balls > 0)
                    {
                        _currentBall = Instantiate(ballPrefab);
                    }
                    else
                    {
                        SwitchState(State.GAMEOVER);
                    }
                }
                if(_currentBall != null && _currentlevel.transform.childCount == 0 && !_isSwitchingState)
                {
                    SwitchState(State.LEVELCOMPLETED);
                }
                break;
            case State.LEVELCOMPLETED:
                break;
            case State.LOADLEVEL:
                break;
            case State.GAMEOVER:
                Destroy(_Ball);
                Destroy(_currentBall);
                Destroy(_currentlevel);
                if(Input.anyKeyDown)
                {   
                    SwitchState(State.MENU);
                }
                break;
        }
    }

    void EndState()
    {
        switch (_state)
        {
            case State.MENU:
                panelMenu.SetActive(false);
                break;
            case State.INIT:
                break;
            case State.PLAY:
                break;
            case State.LEVELCOMPLETED:
                panelLevelComplete.SetActive(false);
                break;
            case State.LOADLEVEL:
                break;
            case State.GAMEOVER:
                panelPlay.SetActive(false);
                panelGameOver.SetActive(false);
                break;
        }
    }
}
