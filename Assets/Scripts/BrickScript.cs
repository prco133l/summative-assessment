using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrickScript : MonoBehaviour
{
    public int hits = 1;
    public int points = 10;
    public Sprite hitMaterial;

    public AudioSource Hit;

    Sprite _orgmaterial;
    SpriteRenderer _renderer;
    // Start is called before the first frame update
    void Start()
    {
        _renderer = GetComponent<SpriteRenderer>();
        _orgmaterial = _renderer.sprite;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {   
        AudioSource.PlayClipAtPoint(Hit.clip, Camera.main.transform.position, 0.08f);
        hits--;
        GameManager.Instance.Score += points;
        if (hits <= 0)
        {
            // Destroy
            Destroy(gameObject);
        }
        _renderer.sprite = hitMaterial;
        Invoke("RestoreMaterial", 0.05f);
    }

    void RestoreMaterial()
    {
        _renderer.sprite = _orgmaterial;
    }
}
