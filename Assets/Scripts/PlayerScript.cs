using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    Rigidbody2D _rigidbody;
    Vector2 position;
    Vector2 mouseDir;
    public float rightScreen;
    public float leftScreen;
    void Start()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        float inputX = Camera.main.ScreenToWorldPoint(new Vector2(Input.mousePosition.x, 0)).x;

        position = new Vector2(inputX, -4);

        
    }

    void FixedUpdate()
    {   
         if(position.x < leftScreen)
        {
            _rigidbody.MovePosition(new Vector2(leftScreen, position.y));
        }
        else if(position.x > rightScreen)
        {
            _rigidbody.MovePosition(new Vector2(rightScreen, position.y));
        }
        else
        {
            _rigidbody.MovePosition(position);
        }
    }
}
